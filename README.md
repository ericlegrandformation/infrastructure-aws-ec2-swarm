# Provision d'une Infra AWS avec Terraform - Ansible - Docker Swarm

![a](./images/AWS-Swarm.png?zoomresize=480%2C240)

Ce projet vise à mettre en service un pipeline CI/CD Jenkins dans le Cloud.

Pour cela, nous allons :
 * Provisionner et configurer une infrastructure CLOUD chez Amazon avec Terraform, Ansible, Docker, Docker Swarm, Portainer 
 * Déployer une stack Jenkins sur notre infra
 * Configurer un pipeline CI/CD Jenkins 

## Pour commencer

Créer et configurer votre compte Amazon AWS 

### Pré-requis

Ce qui est requis pour commencer avec ce projet :

- avoir créer un compte **AWS** :
[https://portal.aws.amazon.com/billing/signup#/start](https://portal.aws.amazon.com/billing/signup#/start)
-  réaliser la configuration de votre compte AWS, notamment:
-  créer un **utilisateur** [https://console.aws.amazon.com/iam/home?region=eu-west-3#/users](https://console.aws.amazon.com/iam/home?region=eu-west-3#/users)
-  créer un **groupe** [https://console.aws.amazon.com/iam/home?region=eu-west-3#/groups](https://console.aws.amazon.com/iam/home?region=eu-west-3#/groups)
-   et la **clé ssh** pour accéder à AWS sans mot de passe : [https://eu-west-3.console.aws.amazon.com/ec2/home?region=eu-west-3#KeyPairs:sort=keyName](https://eu-west-3.console.aws.amazon.com/ec2/home?region=eu-west-3#KeyPairs:sort=keyName)

## Provisionner et configurer une infrastructure CLOUD chez Amazon AWS


Sur votre machine hôte, depuis votre répertoire **/home/'votre nom utilisateur'**, dans un terminal, taper successivement :
-     $ git clone https://framagit.org/ericlegrandformation/infrastructure-aws-ec2-swarm.git
* ouvrer le dossier "infrastructure-aws-ec2-swarm" **avec VSCode**
* ouvrer un **nouveau terminal** dans VSCode
* créer un environnement virtuel python avec la commande 
-     $ virtualenv --python=python3.5 venv
* activer l'environnement virtuel python avec la commande
-     $ source ./venv/bin/activate**"
* installer quelques pré-requis avec la commande 
-     $ sudo apt install libffi-dev python-dev libssl-dev
* installer ansible avec la commande
-     $ pip install ansible
* vérifier la version ansible (version 2.8 mini) avec
-     $ ansible --version

Procéder à l'installation de **Terraform**:

* Terraform n'est pas disponible sous forme de dépôt ubuntu/debian. Pour l'installer il faut le télécharger et l'installer manuellement:
-     $ cp /tmp
-     $ wget https://releases.hashicorp.com/terraform/0.12.6/terraform_0.12.6_linux_amd64.zip
-     $ sudo unzip ./terraform_0.12.6_linux_amd64.zip -d /usr/local/bin/
* puis tester l'installation:
-     $ terraform --version


Tous les outils sont installés, il faut procéder à la configuration maintenant:
* copiez le fichier  `variables.yml.dist`  et renommez le en enlevant le  `.dist`
ajouter les bonnes valeurs pour les variables entre <>
-     ec2_keypair: "<name_ssh_keypair>"
-     ec2_security_group_id: "<your_id_security_group>"
-     ec2_subnet_id: "<your_id_subnet>"

* copiez le fichier  `terraform.tfvars.dist`  et renommez le en enlevant le  `.dist`
ajouter les bonnes valeurs pour les variables entre <>
-     default ="<your_ssh_key_name.pem>"
-     access_key = "<your_access_key>"
-     secret_key = "<your_secret_key>"
-     key_name = "<your_ssh_key_name>"

* copiez le fichier  `ansible.cfg.dist`  et renommez le en enlevant le  `.dist`
ajouter les bonnes valeurs pour les variables entre <>
-     private_key_file=<your_ssh_name.pem>

* copiez le fichier  `inventory/aws_ec2.yml.dist`  et renommez le en enlevant le  `.dist`
ajouter les bonnes valeurs pour les variables entre <>
-     aws_access_key_id: <your_access_key>
-     aws_secret_access_key: <your_secret_access_key>

* copiez le fichier  `inventory/ec2.ini.dist`  et renommez le en enlevant le  `.dist`
ajouter les bonnes valeurs pour les variables entre <>
-     destination_variable = ip-<your-ip>.eu-west-3.compute.internal

* récupérer le fichier de votre **clé ssh** et coller le dans:
-     $ cd /home/"utilisateur"/infrastructure-aws-ec2-swarm
-     $ cd /home/"utilisateur"/infrastructure-aws-ec2-swarm/inventory

Procéder à la création des serveurs:
- Lancer la commande :
-     $ cd /home/"utilisateur"/infrastructure-aws-ec2-swarm
-     $ terraform init
-     $ terraform apply

Si vous rencontrez des messages d'erreurs, vous aurez à définir les export suivants:
-     $ export AWS_ACCESS_KEY_ID='your_access_key'
-     $ export AWS_SECRET_ACCESS_KEY='your_secret_access_key'
-     $ export AWS_REGION=eu-west-3
-     $ export AWS_DEFAULT_REGION=eu-west-3

-     $ export BOTO_USE_ENDPOINT_HEURISTICS=True

-     $ export ANSIBLE_CONFIG="/path_to_your_ansible_cfg_file/ansible.cfg"
-     $ export ANSIBLE_INVENTORY=inventory/ec2.py
-     $ export ANSIBLE_HOSTS=inventory/ec2.py
-     $ export EC2_INI_PATH=inventory/ec2.ini

- Vérifier le bon fonctionnement des serveurs:
-     $ ansible -m ping all

Terminer la configuration des serveurs en executant les **playbook Ansible**:
* Installer Swarm avec la commande
-     $ ansible-playbook -i inventory/aws_ec2.yml install_swarm.yml

Vérifier la configuration des serveurs:
* Allez sur [https://eu-west-3.console.aws.amazon.com/ec2/v2/home?region=eu-west-3#Instances:sort=instanceId](https://eu-west-3.console.aws.amazon.com/ec2/v2/home?region=eu-west-3#Instances:sort=instanceId) , selectionner manager puis cliquer sur "se connecter" pour avoir la commande ssh du type:
-     $ ssh -i "<your_ssh_key_name.pem>" ubuntu@ec2-<ip_master(xx-xx-xx-xx)>.eu-west-3.compute.amazonaws.com
selectionner ensuite worker puis cliquer sur "se connecter" pour avoir la commande ssh du type:
-     $ ssh -i "<your_ssh_key_name.pem>" ubuntu@ec2-<ip_worker(xx-xx-xx-xx)>.eu-west-3.compute.amazonaws.com 

## Installer Portainer sur votre Swarm

Informations extraites de :
[https://portainer.readthedocs.io/en/stable/deployment.html](https://portainer.readthedocs.io/en/stable/deployment.html)
et
[https://github.com/portainer/portainer/issues/2057](https://github.com/portainer/portainer/issues/2057)


Vous connecter en ssh sur le manager avec les commandes
-     $ ssh -i "<your_ssh_key_name.pem>" ubuntu@ec2-<ip_master(xx-xx-xx-xx)>.eu-west-3.compute.amazonaws.com

Télécharger le fichier portainer-agent-stack.yml
-     $ curl -L https://downloads.portainer.io/portainer-agent-stack.yml -o portainer-agent-stack.yml

Corriger ce fichier, entre "images:" et "volumes:", ajouter les lignes suivantes:
-     environment:
        # REQUIRED: Should be equal to the service name prefixed by "tasks." when
        # deployed inside an overlay network
        AGENT_CLUSTER_ADDR: tasks.agent

Déployer la stack portainer
-     $ docker stack deploy --compose-file=portainer-agent-stack.yml portainer

Visiter l'adresse: "ip_serveur_manager:9000", entrer un mot de passe pour votre utilisateur "admin" puis visualiser votre Swarm dans DashBoard, click sur "Go to cluster visualizer"

**Bravo, vous venez de provisionner et de configurer votre infrastructure dans le Cloud .**


## BONUS: Déployer une stack Jenkins sur notre infra

Toujours depuis votre connection ssh sur votre serveur Swarm-manager
-     $ git clone https://framagit.org/ericlegrandformation/jenkins_as_code.git

Déployer la stack Jenkins
-     $ cd jenkins_as_code
-     $ docker stack deploy --compose-file=docker-stack.yml jenkins

Depuis portainer, visualiser votre Swarm dans DashBoard, click sur "Go to cluster visualizer", jenkins-master et jenkins-slave sont installés sur swarm-manager et swarm-worker.

Visiter l'adresse: "ip_serveur_manager:8080", entrer un mot de passe "jenkins" pour votre utilisateur "jenkins" puis entrer dans Jenkins.

**Bravo, vous venez d'installer Jenkins sur votre infrastructure dans le Cloud .**


## Configurer un pipeline CI/CD Jenkins

Depuis Jenkins, clicker sur "Open Blue Ocean"
- Clicker sur "Nouveau Pipeline"
- Clicker sur l'icone "Git"
- Remplir "Repository URL", "Username" et "Password"
- Clicker sur "Create Credential" puis "Create Pipeline"

**Bravo, vous venez de créer un pipeline Jenkins pour votre application.**


## Versions

**Dernière version stable :** 0.1
**Dernière version :** 0.1
Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/terraform_swarm_digitalocean.git)

## Auteurs

* **Eric Legrand** _alias_ [@ericlegrand](https://framagit.org/ericlegrandformation/infrastructure-aws-ec2-swarm.git)
avec adaptation du rôle thomasjpfan.docker-swarm.
