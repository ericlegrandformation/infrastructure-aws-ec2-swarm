variable "manager_instance_count" {
  type=number
  default =1
  }

variable "worker_instance_count" {
  type=number
  default =1
  }

variable "ssh_user_name" {
  type="string"
  default ="ubuntu"
}

variable "ssh_key_path" {
  type="string"
  default ="ubuntuT2micro.pem"
}

provider "aws" {
  access_key = "AKIAVGLANE3Z36T5NTPV"
  secret_key = "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"
  region     = "eu-west-3"
}

# Create new aws security group for appliaction instance. 
# Only HTTPS,HTTP and SSH ports is open for outside.
resource "aws_security_group" "app_sg" {
    name            = "ssh_http_https"
    description     = "For web and ssh access"

    ingress {  #HTTP Port
            from_port       = 80
            to_port         = 80
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

    ingress {  #HTTP Port
            from_port       = 8080
            to_port         = 8080
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

        ingress {  #HTTP Port
            from_port       = 9000
            to_port         = 9000
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

    ingress {  #HTTP Port
            from_port       = 5000
            to_port         = 5000
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

    ingress {  #HTTP Port
            from_port       = 5001
            to_port         = 5001
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

    
    ingress {  #SSH Port
            from_port       = 22
            to_port         = 22
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

    ingress {  #HTTPS Port
            from_port       = 443
            to_port         = 443
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

    ingress {  #Docker Port
            from_port       = 2376
            to_port         = 2376
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

    ingress {  #Docker Port
            from_port       = 2377
            to_port         = 2377
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }
    
    ingress {  #Docker Port
            from_port       = 7946
            to_port         = 7946
            protocol        = "tcp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

        ingress {  #Docker Port
            from_port       = 7946
            to_port         = 7946
            protocol        = "udp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

    ingress {  #Docker Port
            from_port       = 4789
            to_port         = 4789
            protocol        = "udp"
            cidr_blocks     = ["0.0.0.0/0"]

        }

    egress  {  #Outbound all allow
            from_port       = 0
            to_port         = 0
            protocol        = -1
            cidr_blocks     = ["0.0.0.0/0"]
        }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "managers" {
  count = "${var.manager_instance_count}"
  ami           = "ami-098fa516965100b01"
  instance_type = "t2.micro"
  key_name = "ubuntuT2micro" #KeyPair name to be attached to the instance.
  vpc_security_group_ids = ["${aws_security_group.app_sg.id}"]    #Security group id which we already created

  tags = {
    Name = "manager",
    "manager" = "swarm_nodes"
  }
}

resource "null_resource" "ProvisionRemoteHostsManagersIpToAnsibleHosts" {
  count = "${var.manager_instance_count}"
  connection {
    type = "ssh"
    user = "${var.ssh_user_name}"
    host = "${element(aws_instance.managers.*.public_ip, count.index)}"
    private_key = "${file("${var.ssh_key_path}")}"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt -y update",
      "sudo apt -y install python-setuptools",
      "sudo apt -y install python-pip",
      "sudo apt -y install python3-pip",
      "sudo pip install httplib2",
      "sudo pip3 install httplib2"
    ]
  }

}

resource "aws_instance" "workers" {
  count = "${var.worker_instance_count}"
  ami           = "ami-098fa516965100b01"
  instance_type = "t2.micro"
  key_name = "ubuntuT2micro" #KeyPair name to be attached to the instance.
  vpc_security_group_ids = ["${aws_security_group.app_sg.id}"]    #Security group id which we already created

  tags = {
    Name = "worker"
    "worker" = "swarm_nodes"
  }
}

resource "null_resource" "ProvisionRemoteHostsWorkersIpToAnsibleHosts" {
  count = "${var.worker_instance_count}"
  connection {
    type = "ssh"
    user = "${var.ssh_user_name}"
    host = "${element(aws_instance.workers.*.public_ip, count.index)}"
    private_key = "${file("${var.ssh_key_path}")}"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt -y update",
      "sudo apt -y install python-setuptools",
      "sudo apt -y install python-pip",
      "sudo pip install httplib2"
    ]
  }

}

